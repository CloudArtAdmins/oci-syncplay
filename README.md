# oci-syncplay

Containerized syncplay server setup.

As per usual: copy `.env.template` to `.env`, fill in your environment variables, and run the script.

This script assumes the HTTPS proxy was already run and obtained SSL certs.
