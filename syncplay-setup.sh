#!/bin/sh

podman pod create  \ 
	-p 8999:8999 \ 
	--hostname $SYNCPLAY_SUBDOMAIN.$DOMAIN \
	--name pod-syncplay

podman run \
  --rm -d \
  --name syncplay \
  --pod pod-syncplay \
  --env PASSWORD=$SYNCPLAY_PASSWORD \
  --env TLS=/certs/live/$DOMAIN \
  --volume letsencrypt-certs:/certs:ro,z \
  --user 0:0 \
  docker.io/ninetaillabs/syncplay-server


